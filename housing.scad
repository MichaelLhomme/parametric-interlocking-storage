include <BOSL2/std.scad>

$railEndStopLength = 3;
$endStopRounding = 3;
$endStopWidth = 8;

module housing_base(width, height) {
    cube([width, height, $baseLength], anchor=BOTTOM);
}

// TODO round large width
module rail(color="lightblue") {
    back($railBaseWidth / 2)
        color(color)
        linear_extrude(height=$baseLength)
        trapezoid(h=$railBaseHeight, w1=$railBaseWidth, w2=$railLargeWidth);
}

module rails(referenceWidth, withEndstop, color="lightblue", extraClearance = $extraClearance) {
    baseTranslate = referenceWidth / 4;
    spacingWidth = extraClearance + ($railLargeWidth + $railBaseWidth);
    
    // bi rails
    union() {
        right(baseTranslate - spacingWidth / 2) rail(color=color);
        right(baseTranslate + spacingWidth / 2) rail(color=color);

        // endstop
        if(withEndstop)
            back(0.001)
            right(baseTranslate)
            color("red")
            wedge([spacingWidth, $railBaseWidth, $railEndStopLength], anchor=BOTTOM+FRONT);

        // mono rail
        difference() {
            left(baseTranslate)
                rail(color=color);

            if(!withEndstop)
                back(0.001)
                left(baseTranslate)
                color("red")
                wedge([$railLargeWidth, $railBaseWidth, $railEndStopLength], spin=180, anchor=BOTTOM+BACK);
        }
    }
}

module drawer_end_stop(color="green") {
    color(color)
      hull() {
          cube([$endStopWidth, 0.001, $endStopWidth], anchor=CENTER);
          fwd($endStopRounding / 2) xcyl(d=$endStopRounding, h=$endStopWidth);
      }
}

module simple_housing(width = $baseWidth, height = $baseHeight) {
    union() {
        // rails up and down
        zrot(0)
          back(height / 2)
          rails(referenceWidth = width, withEndstop = true);

        zrot(180)
          back(height / 2)
          rails(referenceWidth = width, withEndstop= false);
            
        
        // rails left and right
        zrot(90)
          back(width / 2)
          rails(referenceWidth = height, withEndstop = false);

        zrot(270)
          back(width / 2)
          rails(referenceWidth = height, withEndstop = true);
        
        difference() {
            housing_base(width = width, height = height);
        
            // drawer endstop
            up($baseLength - ($endStopWidth / 2) - ($baseLength / 10))
                back(height / 2 + 0.01)
                drawer_end_stop();
        
            // drawer rest stop
            zrot_copies(rots=[0, 180])
                up(height / 2)
                left(width / 2)
                color("magenta")
                sphere(r=$restStopRadius);
        }
    }
}

module housing(width = $baseWidth, height = $baseHeight, x = 1, y = 1) {
    union() {
        // rails up and down
        zrot(0)
            back(height * y / 2 + ($railBaseHeight / 2) * (y - 1)) 
            xcopies(spacing = width + $railBaseHeight, n = x)
            rails(referenceWidth = width, withEndstop = true, color = "lightgreen");

        zrot(180)
            back(height * y / 2 + ($railBaseHeight / 2) * (y - 1)) 
            xcopies(spacing = width + $railBaseHeight, n = x)
            rails(referenceWidth = width, withEndstop = false, color = "lightgreen");
        
        // rails left and right
        zrot(90)
            back(width * x / 2 + ($railBaseHeight / 2) * (x - 1))
            xcopies(spacing = height + $railBaseHeight, n = y)
            rails(referenceWidth = height, withEndstop = false, color = "lightgreen");

        zrot(270)
            back(width * x / 2 + ($railBaseHeight / 2) * (x - 1))
            xcopies(spacing = height + $railBaseHeight, n = y)
            rails(referenceWidth = height, withEndstop = true, color = "lightgreen");
        
        difference() {
            housing_base(
                height = height * y + $railBaseHeight * (y - 1),
                width = width * x + $railBaseHeight * (x - 1)
            );
        
            // drawer endstop
            up($baseLength - ($endStopWidth / 2) - ($baseLength / 10))
              back(0.01 + height * y / 2 + ($railBaseHeight / 2) * (y - 1))
              drawer_end_stop();
        
            // drawer rest stop
            color("magenta")
              zrot_copies(rots=[0, 180])
              ycopies(spacing = height, n = y)
              up(height / 2)
              left(width * x / 2 + ($railBaseHeight / 2) * (x - 1))
              sphere(r=$restStopRadius);
        }
    }
}



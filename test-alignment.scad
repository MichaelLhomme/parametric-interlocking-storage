include <BOSL2/std.scad>

use <./housing.scad>

module test_alignment() {
    testWidth = 70;
    testHeight = 40;
    hSpacing = testWidth + $railBaseHeight;
    vSpacing = testHeight + $railBaseHeight;
    
    // reference top row
    xcopies(n=3, spacing = hSpacing)
        simple_housing(width = testWidth, height = testHeight);
    
    // reference bottom row
    fwd(vSpacing * 4)
        xcopies(n=3, spacing = hSpacing)
        simple_housing(width = testWidth, height = testHeight);
    
    // reference left col
    left(hSpacing * 2)
        fwd(vSpacing * 2)
        ycopies(n=3, spacing = vSpacing)
        simple_housing(width = testWidth, height = testHeight);
    
    // reference right col
    right(hSpacing * 2)
        fwd(vSpacing * 2)
        ycopies(n=3, spacing = vSpacing)
        simple_housing(width = testWidth, height = testHeight);
    
    // block
    fwd(vSpacing * 2)
        housing(x = 3, y = 3, width = testWidth, height = testHeight);
}



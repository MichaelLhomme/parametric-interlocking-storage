include <BOSL2/std.scad>

$roundingBack = 5;
$drawerHousingClearance= 1.5;

$pathRadius = 1;
$pathXMargin = 3;
$pathYMargin = 3;


// TODO support splitter

module drawer_base(width, height, length) {
  rounding = $roundingBack;

  fwd(length / 2)
  linear_extrude(height = height)
  hull() {
    rect([width, 1], anchor=BOTTOM);
    back(length - rounding) {
      left((width / 2) - rounding) circle(r=rounding);
      right((width / 2) - rounding) circle(r=rounding);
    }
  } 
}

module drawer_rest_stop(width, height) {
    up(height / 2) {

      // rest stop
      color("green")
          fwd($pathRadius * 1.5 + width / 2) 
          sphere(r=$restStopRadius);
      
      // horizontal access cylinder
      fwd(width / 2 - $restStopRadius) {
        color("lightgreen")
            sphere(r=$restStopRadius);
        
        color("orange")
            ycyl(r=$restStopRadius, h=width, anchor=FRONT);
      }
    }
}

module drawer_details_side(width, height) {
    r = $pathRadius;
    xMargin = $pathXMargin;
    yMargin = $pathYMargin;
    workingWidth = width - xMargin;
    workingHeight = height - yMargin;
    wHalf = workingWidth / 2;
    hHalf = height / 2;
    xMove = wHalf - xMargin;
    yMove = hHalf - yMargin;

    color("magenta")
    fwd($pathXMargin / 2)
    up(hHalf) {
      // vertical left line
      fwd(xMove) up(yMove) sphere(r=r);
      fwd(xMove) down(yMove) sphere(r=r);
      fwd(xMove) zcyl(r=r, h=height - yMargin * 2);

      // horizontal lines
      up(yMove) ycyl(r=r, h=workingWidth - xMargin * 2);
      down(yMove) ycyl(r=r, h=workingWidth - xMargin * 2);

      // top small vertical line
      back(xMove) up(yMove) sphere(r=r);
      back(xMove) up(yMove * 1 / 3) sphere(r=r);
      back(xMove) up(yMove) zcyl(r=r, h=yMove * 2 / 3, anchor=TOP);

      // bottom small vertical line
      back(xMove) down(yMove) sphere(r=r);
      back(xMove) down(yMove * 1 / 3) sphere(r=r);
      back(xMove) down(yMove) zcyl(r=r, h=yMove * 2 / 3, anchor=BOTTOM);

      // middle vertical line
      zcyl(r=r, h=height - yMargin * 2);
    }
}

module drawer_details_back(width, height) {
    r = $pathRadius;
    xMargin = $pathXMargin + $roundingBack;
    yMargin = $pathYMargin;
    wHalf = width / 2;
    hHalf = height / 2;
    xMove = wHalf - xMargin;
    yMove = hHalf - yMargin;

    color("magenta")
    up(hHalf) {
      // vertical left line
      fwd(xMove) up(yMove) sphere(r=r);
      fwd(xMove) down(yMove) sphere(r=r);
      fwd(xMove) zcyl(r=r, h=height - yMargin * 2);

      // vertical right line
      back(xMove) up(yMove) sphere(r=r);
      back(xMove) down(yMove) sphere(r=r);
      back(xMove) zcyl(r=r, h=height - yMargin * 2);

      // horizontal lines
      up(yMove) ycyl(r=r, h=width - xMargin * 2);
      down(yMove) ycyl(r=r, h=width - xMargin * 2);
    }
}

module drawer_details_front(width, height) {
    r = $pathRadius;
    xMargin = $pathXMargin;
    yMargin = $pathYMargin;
    wHalf = width / 2;
    hHalf = height / 2;
    xMove = wHalf - xMargin;
    yMove = hHalf - yMargin;

    color("blue")
    up(hHalf)
    zrot(90) {
      // vertical right line
      fwd(xMove) up(yMove) sphere(r=r);
      fwd(xMove) down(yMove) sphere(r=r);
      fwd(xMove) zcyl(r=r, h=height - yMargin * 2);

      // vertical left line
      back(xMove) up(yMove) sphere(r=r);
      back(xMove) down(yMove) sphere(r=r);
      back(xMove) zcyl(r=r, h=height - yMargin * 2);

      // horizontal lines
      ycyl(r=r, h=width - xMargin * 2);
      down(yMove) ycyl(r=r, h=width - xMargin * 2);
    }
}

module drawer_pick(w, h, l) {
    largeWidth = h / 2 - 5;
    largeHeight = w / 2;
    smallWidth = 3;
    smallHeight = w / 3;

    color("red")
        prismoid(size1=[largeWidth, largeHeight], size2=[smallWidth, smallHeight], h=8, shift=[(largeWidth - smallWidth) / 2 , 0], anchor=RIGHT+BOTTOM, orient=FRONT, spin=90, rounding=1);
}

module simple_drawer(width = $baseWidth, height = $baseHeight, length = $baseLength, clearance = $drawerHousingClearance) {
    drawerWidth = width - clearance * 2;
    drawerHeight = height - clearance * 2;
    drawerLength = length - clearance * 2;
    
    union() {
        difference() {
            drawer_base(width = drawerWidth, height = drawerHeight, length = drawerLength);

            // Left side
            left(drawerWidth / 2) {
              drawer_details_side(width = drawerLength, height = drawerHeight);
              back(clearance * 2 + drawerLength - height / 2) drawer_rest_stop(width = drawerLength, height = drawerHeight);
            }

            // Right side
            right(drawerWidth / 2) {
              drawer_details_side(width = drawerLength, height = drawerHeight);
              back(clearance * 2 + drawerLength - height / 2) drawer_rest_stop(width = drawerLength, height = drawerHeight);
            }

            // Back side
            back(drawerLength / 2)
              zrot(90)
              drawer_details_back(width = drawerWidth, height = drawerHeight);
        }

        // Front side
        fwd(drawerLength / 2)
          drawer_details_front(width = drawerWidth, height = drawerHeight);
    
        // Pick
        up(drawerHeight)
          fwd(drawerLength / 2)
          drawer_pick(w = drawerWidth, h = height, l = drawerLength);
    }
}

module drawer(width = $baseWidth, height = $baseHeight, length = $baseLength, x = 1, y = 1, clearance = $drawerHousingClearance) {
    drawerWidth = (width * x) - clearance * 2;
    drawerHeight = (height  * y) - clearance * 2;
    drawerLength = length - clearance * 2;
    
    union() {
        difference() {
            drawer_base(width = drawerWidth, height = drawerHeight, length = drawerLength);

            // Left side
            left(drawerWidth / 2) {
              drawer_details_side(width = drawerLength, height = drawerHeight);
              back(clearance * 2 + drawerLength - height / 2) drawer_rest_stop(width = drawerLength, height = drawerHeight);
            }

            // Right side
            right(drawerWidth / 2) {
              drawer_details_side(width = drawerLength, height = drawerHeight);
              back(clearance * 2 + drawerLength - height / 2) drawer_rest_stop(width = drawerLength, height = drawerHeight);
            }

            // Back side
            back(drawerLength / 2)
              zrot(90)
              drawer_details_back(width = drawerWidth, height = drawerHeight);
        }

        // Front side
        fwd(drawerLength / 2)
          drawer_details_front(width = drawerWidth, height = drawerHeight);
    
        // Pick
        up(drawerHeight)
          fwd(drawerLength / 2)
          drawer_pick(w = drawerWidth, h = height, l = drawerLength);
    }
}


include <BOSL2/std.scad>

use <./housing.scad>

module demo_large() {
    zdistribute(spacing = $baseLength * 1.5) {
        simple_housing();
        housing(x = 1);
        housing(x = 2);
        housing(x = 3);
    }
}

module demo_tall() {
    zdistribute(spacing = $baseLength * 1.5) {
        simple_housing();
        housing(y = 1);
        housing(y = 2);
        housing(y = 3);
    }
}

module demo_combo() {
    zdistribute(spacing = $baseLength * 1.5) {
        simple_housing();
        housing(x = 2, y = 1);
        housing(x = 2, y = 2);
        housing(x = 1, y = 2);
        housing(x = 3, y = 2);
    }
}



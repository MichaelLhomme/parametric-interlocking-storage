$baseWidth = 45;
$baseHeight = 45;
$baseLength = 80;

$railBaseWidth = 2.5;
$railBaseHeight = 2.5;
$railLargeWidth = 6;

$restStopRadius = 2;

$extraClearance = 0.1;

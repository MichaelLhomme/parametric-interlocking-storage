include <BOSL2/std.scad>

include <./parameters.scad>

$fa = 1;
$fn = 50;
$fs = 0.4;

use <./housing.scad>
simple_housing();
left(75) housing(x = 1, y = 1);
right(100) housing(x = 2, y = 1);
right(250) housing(x = 3, y = 1);

use <./drawer.scad>
//simple_drawer();
//left(75) drawer(x = 1, y = 1);
//right(100) drawer(x = 2, y = 1);
//right(250) drawer(x = 3, y = 1);

use <./housing-demo.scad>
//demo_combo();
//demo_tall();
//demo_large();

use <./test-alignment.scad>
//test_alignment();
